def make_it_count(func, counter_name):
    def new_func(*args, **kwargs):
        globals()[counter_name] += 1
        return func(*args, **kwargs)
    return new_func




var = 0
function = make_it_count(round, 'var')

function(4.5948)
function(3.111)
print(var)