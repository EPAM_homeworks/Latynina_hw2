from string import ascii_lowercase

def letters_range(*args):
    
    letters = list(ascii_lowercase)
    length = len(args)
    alphabet = []
    i,j = 0, 0
    step = 1
    start = 'a'
    
    if length == 3:
        start, stop, step = args
    if length == 2:
        a, b = args
        if isinstance(b, int):
            stop = a
            step = b
        else:
            start = a
            stop = b
    if length == 1:
        stop = args[0]
    
    while j<26:
        while letters[i]!=start:
            i+=1
            j+=1
        if step>0 and letters[j]>=stop:
            break
        if step<0 and letters[j]<=stop:
            break
        alphabet.append(letters[j])
        j+=step
    return alphabet