def atom(variable=None):
    
    def get_value():
        nonlocal variable
        return variable
    
    def set_value(new=None):
        nonlocal variable
        variable = new
        return variable
    
    def process_value(*funcs):
        nonlocal variable
        variables = []
        for func in funcs:
            variables.append(func(variable))
        return variables
    
    return get_value, set_value, process_value

gett, sett, process = atom(46)
print('Current value: ',gett())
print('Setting a new value: ', sett(2))
print('The result of different manipulations: ', process(lambda x: x-2, lambda x: x**4, lambda x: x//10))