import inspect
import sys

def partial(func, *fixated_args, **fixated_kwargs):
    if not fixated_args and not fixated_kwargs:
        return func
    try:
        keys = list(map(lambda x: x.lstrip(), str(inspect.signature(func)).strip('()').split(',')))
        arg = {}
        for i, key in enumerate(keys):
            try:
                if '=' in key:
                    future_dict = key.split('=')
                    arg[future_dict[0]] = future_dict[1]
                else:
                    arg[key] = fixated_args[i]
            except IndexError:
                arg[key] = 'defined in main'
    except ValueError:
        keys = None

    def modified_func(*fargs, **fkwargs):
        updated_kwargs = fixated_kwargs.copy()
        updated_kwargs.update(fkwargs)
        return func(*fixated_args, *fargs, **updated_kwargs)
    modified_func.func = func
    modified_func.args = fixated_args
    modified_func.kwargs = fixated_kwargs
    modified_func.__name__ = 'partial_'+func.__name__
    
    if keys is None:
        docs = str(fixated_args).strip('()') + '\n'
        for key, value in fixated_kwargs.items():
            docs += f'{key} = {value}\n'
    else:
        for key, value in fixated_kwargs.items():
            for k in arg.keys():
                if key == k:
                    arg[key] = value
        docs = ''
        for var, value in arg.items():
            docs += f'{var} = {value}\n'
        
    
    modified_func.__doc__ = modified_func.__doc__ = "A partial implementation of "+func.__name__+" with pre-applied arguments being:\n"+docs
    return modified_func


def func(a, b, kwarg1=4, kwarg2=3):
    print(a, b, kwarg1, kwarg2)
    
f=partial(round, ndigits=2)
f(2.66893)
print(f.__doc__)
part_func = partial(func, 99, kwarg1=66)
part_func(777)
print(part_func.__doc__)